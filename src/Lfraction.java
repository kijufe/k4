/** This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {


   /**
    * Main method. Different tests.
    * */
   public static void main (String[] param) {
      System.out.println(Lfraction.valueOf("2/-4"));
   }

   private long numerator;
   private long denominator;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      if (b == 0) throw new RuntimeException("denominator can't be 0");
      long gcm = gcm(a, b);
      this.numerator = a / gcm;
      this.denominator = b / gcm;
      if (this.numerator > 0 && this.denominator < 0) {
         this.numerator = 0 - this.numerator;
         this.denominator = Math.abs(this.denominator);
      }
   }

   /** Public method to access the numerator field.
    * @return numerator
    */
   public long getNumerator() {
      return this.numerator;
   }

   /** Public method to access the denominator field.
    * @return denominator
    */
   public long getDenominator() {
      return this.denominator;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return this.numerator + "/" + this.denominator;
   }


   /** Equality test.
    * @param o second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Lfraction lfraction = (Lfraction) o;
      return compareTo(lfraction) == 0;
   }


   public static long gcm(long a, long b) {
      return b == 0 ? a : Math.abs(gcm(b, a % b));
   }


   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      final long prime = 31;
      long result = 1;
      result = prime * result + numerator;
      result = prime * result + denominator;
      return (int) result;
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m)
   {
      return new Lfraction(this.numerator * m.denominator + m.numerator * this.denominator, this.denominator * m.denominator);
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      long gcm = gcm(this.numerator * m.numerator, this.denominator * m.denominator);
      return new Lfraction(this.numerator * m.numerator / gcm, this.denominator * m.denominator / gcm);
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      if (numerator == 0) throw new RuntimeException("denominator can't be 0");
      long den = (numerator < 0) ? 0 - denominator : denominator;
      return new Lfraction(den, Math.abs(this.numerator));
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(- this.numerator, this.denominator);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
      return plus(m.opposite());
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      if (m.numerator == 0 || m.denominator == 0) throw new RuntimeException("Can't divide by 0");
      return times(m.inverse());
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      if (this.numerator * m.denominator == this.denominator * m.numerator) return 0;
      else if (this.numerator * m.denominator <  this.denominator * m.numerator) return -1;
      else return 1;
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(this.numerator, this.denominator);
   }


   /** Integer part of the (improper) fraction.
    * @return integer part of this fraction
    */
   public long integerPart() {
      return this.numerator / this.denominator;
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      return toLfraction(toDouble() - integerPart(), this.denominator);
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return (double) numerator / denominator;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      return new Lfraction((long) Math.ceil(f * d), d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toSting) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      if (!s.contains("/")) throw new RuntimeException("String " + s + " has wrong format (digit + / + digit");
      if (s.split("/").length > 2) throw new RuntimeException("String " + s + " has too much arguments");
      if (!s.split("/")[0].matches("\\d+|-\\d+")) throw new RuntimeException("argument " + s.split("/")[0] + " is illegal in string " + s);
      if (!s.split("/")[1].matches("\\d+|-\\d+")) throw new RuntimeException("argument " + s.split("/")[1] + " is illegal in string " + s);
      if (!s.matches("^-?\\d+/-?\\d+$")) throw new RuntimeException("String " + s + " has wrong format");

      return new Lfraction(Long.parseLong(s.split("/")[0]), Long.parseLong(s.split("/")[1]));
   }
}